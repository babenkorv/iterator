<?php

require_once 'PhoneCollection.php';

echo '<pre>';

$phones = ['0999', '09992', '12323', '31231'];

//Iterator interface implementation
$phonesCollection = new PhoneCollection($phones);
var_export($phonesCollection->getAll());
echo PHP_EOL;

foreach ($phonesCollection as $key => $value) {
    echo $key . ' : ' . $value . PHP_EOL;
}

$phonesCollection->rewind();
while ($phonesCollection->valid()) {
    $key = $phonesCollection->key();
    $value = $phonesCollection->current();
    echo $key . ' : ' . $value . PHP_EOL;

    $phonesCollection->next();
}


//IteratorAggregate interface implementation
echo PHP_EOL . 'IteratorAggregateImpl' . PHP_EOL;

require_once 'IteratorAggregateImpl.php';
$phonesCollection = new IteratorAggregateImpl($phones);
foreach ($phonesCollection as $key => $value) {
    echo $key . ' : ' . $value . PHP_EOL;
}

//Iterator array
echo PHP_EOL . 'Iterator array' . PHP_EOL;

require_once 'ArrayIteratorImpl.php';
$phonesCollection = new ArrayIteratorImpl($phones);
foreach ($phonesCollection as $key => $value) {
    echo $key . ' : ' . $value . PHP_EOL;
}
//Iterator array
echo PHP_EOL . 'ArrayIteratorImplAccess' . PHP_EOL;

require_once 'ArrayIteratorImplAccess.php';

$phonesCollection = new ArrayIteratorImplAccess($phones);

$phonesCollection['wqe'] = 228;
unset($phonesCollection[0]);
echo $phonesCollection['wqe'];

foreach ($phonesCollection as $key => $value) {
    echo $key . ' : ' . $value . PHP_EOL;
}

echo PHP_EOL . ' count : ' . count($phonesCollection);