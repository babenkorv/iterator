<?php


class PhoneCollection implements Iterator
{
    private $phones = [];
    private $iteratorState = 0;

    public function __construct(array $phones)
    {
        $this->phones = $phones;
    }

    public function add(string $phone)
    {
        $this->phones[] = $phone;
    }

    public function getAll()
    {
        return $this->phones;
    }

    /**
     * @inheritDoc
     */
    public function current()
    {
        return $this->phones[$this->key()];
    }

    /**
     * @inheritDoc
     */
    public function next()
    {
        $this->iteratorState++;
    }

    /**
     * @inheritDoc
     */
    public function key()
    {
        return $this->iteratorState;
    }

    /**
     * @inheritDoc
     */
    public function valid()
    {
        return ($this->key() < count($this->getAll()));
    }

    /**
     * @inheritDoc
     */
    public function rewind()
    {
        $this->iteratorState = 0;
    }
}