<?php


class ArrayIteratorImpl implements IteratorAggregate
{
    private $phones = [];

    public function __construct(array $phones)
    {
        $this->phones = $phones;
    }

    public function getIterator()
    {
        return new ArrayIterator(array_values($this->phones));
    }
}