<?php

class ArrayIteratorImplAccess implements IteratorAggregate, ArrayAccess, Countable
{
    private $phones = [];

    public function __construct(array $phones)
    {
        $this->phones = $phones;
    }

    public function getIterator()
    {
        return new ArrayIterator(array_values($this->phones));
    }

    public function offsetExists($offset)
    {
        return array_key_exists($offset, $this->phones);
    }

    public function offsetGet($offset)
    {
        return $this->phones[$offset];
    }

    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->phones[] = $value;
        } else {
            $this->phones[$offset] = $value;
        }
    }

    public function offsetUnset($offset)
    {
        unset($this->phones[$offset]);
    }

    /**
     * @inheritDoc
     */
    public function count()
    {
        return count($this->phones);
    }
}