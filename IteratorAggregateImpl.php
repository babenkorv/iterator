<?php

class PhonesIterator implements Iterator
{
    private $phones = [];
    private $iteratorState = 0;

    public function __construct(array $phones)
    {
        $this->phones = $phones;
    }

    public function current()
    {
        return $this->phones[$this->key()];
    }

    public function next()
    {
        $this->iteratorState++;
    }

    public function key()
    {
        return $this->iteratorState;
    }

    public function valid()
    {
        return ($this->key() < count($this->phones));
    }

    public function rewind()
    {
        $this->iteratorState = 0;
    }
}

class IteratorAggregateImpl implements IteratorAggregate
{
    private $phones = [];

    public function __construct(array $phones)
    {
        $this->phones = $phones;
    }

    public function getIterator()
    {
        return new PhonesIterator($this->phones);
    }
}

